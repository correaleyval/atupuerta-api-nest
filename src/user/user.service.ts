import { Injectable } from '@nestjs/common';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/user.dto';

import * as bcrypt from 'bcrypt';

const SALT_WORK_FACTOR = Number.parseInt(process.env.SALT) || 10

@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<User>
    ) { }

    async getUsers(): Promise<User[]> {
        const users = await this.userModel.find()

        return users
    }

    async getUsersByQuery(query: {}): Promise<User[]> {
        const users = await this.userModel.find(query)

        return users
    }

    async getUser(username: string): Promise<User> {
        const user = await this.userModel.findOne({ username })

        return user
    }

    async createUser(newUser: CreateUserDTO): Promise<User> {
        const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)

        newUser.password = await bcrypt.hash(newUser.password, salt)

        const user = await this.userModel.create(newUser)

        return user
    }

    async deleteUser(userID: string): Promise<User> {
        const user = this.userModel.findByIdAndDelete(userID)

        return user
    }

    async updateUser(userID: string, newUserData: CreateUserDTO): Promise<User> {

        if (newUserData.password) {
            const salt = await bcrypt.genSalt(SALT_WORK_FACTOR)

            newUserData.password = await bcrypt.hash(newUserData.password, salt)
        }

        const user = await this.userModel.findByIdAndUpdate(userID, newUserData, { new: true })

        return user
    }
}
