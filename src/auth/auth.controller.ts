import { Controller, Request, Post, Get, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';

import { JwtAuthGuard } from './jwt-auth.guard';
import { ApiTags, ApiBearerAuth, ApiBody, ApiProperty } from '@nestjs/swagger';

class UserLogin {
  @ApiProperty({
    description: 'Login username',
    required: true
  })
  readonly username: string
  @ApiProperty({
    description: 'Login password',
    required: true
  })
  readonly password: string
}

@Controller('auth')
@ApiTags('auth')
export class AuthController {

  constructor(private authService: AuthService) { }

  // /auth/login
  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiBody({
    type: UserLogin
  })
  async login(@Request() req) {

    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  @ApiBearerAuth()
  getProfile(@Request() req) {
    return req.user;
  }
}
